<?php 
  include_once 'easebuzz/easepay-lib.php';
  // print_r($_POST);
  if(isset($_POST['status'])){  
    $SALT='HGH49I4N9U';
    $result = response( $_POST, $SALT );  
  }
?>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta name="author" content="ajinkya pawar"/>
<meta name="rating" content="general" />
<meta name="distribution" content="global"/>
<meta name="doc-type" content="Public"/>
<meta name="distribution" content="Global"/>
<meta name="classification" content="travel"/>
<meta name="geo.country" content="in"/>
<meta name="geo.region" content="IN-Maharashtra" />
<meta name="geo.placename" content="Vishrantwadi, Pune, Maharashtra, India"/>
<meta name="geo.position" content="18.574546;73.889210" />
<meta name="ICBM" content="18.574546, 73.889210" />
<meta name="revisit-after" content="7 days">
<meta name=viewport content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property='article:publisher' content='https://www.facebook.com/pawartravels'/>
<meta property='twitter:account_id' content='1442932658'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="p:domain_verify" content="17075096d1a0f34463341555fd6458ad"/>
<meta name="alexaVerifyID" content="g6tWmyWy_Ez39MICPsKCPBPLzJ0"/>
<meta name="google-site-verification" content="Db_TcUpAKNRMACMovA6E306TFww_nBw7gTW2tOMKJyo" />
<meta name="msvalidate.01" content="3CC115F5FAF8B434A49422F90D16FA16" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
<title>Payment Process</title>
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style-slider.css" rel="stylesheet">
<meta name="description" content="Pawar Travels was established in 2004,it's one of the reliable name in the pune to mumbai airport drop more than ten years of Experience Car Rental">
<meta name="keywords" content="Pune to shirdi, pune to mumbai, car hire,car rental services,tourist places">
<meta name="robots" content="index, follow"></head>
<body>
<div class="container">
<div class="row" style="background-color:#fff; color:#42a9f0; padding:10px;">
<div class="col-md-5 col-sm-5">
<span style="font-size:16px; margin-top:3px;"><i class="fa fa-phone-square"></i>
9822550396 &nbsp;&nbsp; 9325668721 </span>
</div>
<div class="col-md-4 col-sm-4">
<span style="font-size:16px;  margin-top:3px;"><i class="fa fa-envelope-square"></i>	<a href="mailto:23ajaypawar@gmail.com
">23ajaypawar@gmail.com</a></span>
</div>
<div class="col-md-3 col-sm-3">
<ul class="social-icons">
<li><span style="font-size:15px;"><a href="https://www.facebook.com/pawartravels" target="_blank" class="fa fa-facebook"></a></span></li>
<li><span style="font-size:15px;"><a href="https://plus.google.com/u/0/102246497407922758833
" target="_blank" class="fa fa-google-plus"></a></span></li>
<li><span style="font-size:15px;"><a href="https://www.youtube.com/user/pawartravels" target="_blank" class="fa fa-youtube"></a></span></li>
</ul>
</div>
</div>
</div>
<div class="site-header">
<div class="container">
<div class="main-header">
<div class="row">
<div class="col-md-3 col-sm-5 col-xs-9">
<div class="logo">
<img src="images/logo.png" alt="Pawar Travels" title="Pawar
Travels" class="img-responsive">
</div> 
</div> 
<div class="col-md-9 col-sm-7 col-xs-3">
<div class="main-menu">
<ul class="visible-lg visible-md">
<li><a href="index.html">Home</a></li>
<li><a href="about-us.html">About Us</a></li>
<li><a href="car-rental-packages.html">Packages</a></li>
<li><a href="http://carrentalsinpune.blogspot.in/" target="_blank">Blog</a></li>
<li><a href="enquiry-booking-cab-hire.php">Enquiry</a></li>
<li><a href="contact-us.php">Contact</a></li>
<li class="active"><a href="payment.php">Payment</a></li>
</ul>
<a href="#" class="toggle-menu visible-sm visible-xs">
<i class="fa fa-bars"></i>
</a>
</div> 
</div> 
</div> 
</div> 
<div class="row">
<div class="col-md-12 visible-sm visible-xs">
<div class="menu-responsive">
<ul>
<li><a href="index.html">Home</a></li>
<li class="active"><a href="about-us.html">About Us</a></li>
<li><a href="car-rental-packages.html">Packages</a></li>
<li><a href="http://carrentalsinpune.blogspot.in/" target="_blank">Blog</a></li>
<li><a href="enquiry-booking-cab-hire.php">Enquiry</a></li>
<li><a href="contact-us.php">Contact</a></li>
<li><a href="payment.php">Payment</a></li>
</ul>
</div>
</div> 
</div> 
</div> 
</div> 
<div class="page-top" id="templatemo_about"> 
</div>
<div class="middle-content">
<div class="container">
	<div class="row">
		<div class="col-lg-7 col-sm-7 address">
          <h4>
            Submit your detail information
          </h4>
		    <div class="contact-form">
          <?php 
          if(isset($_POST['status'])){
              if($_POST['status'] == 'failure'){
                echo '<span>Something Went Wrong, Please Try Again</span>';
              }else{
                echo '<span>Thank for your Support</span>';
              }
          }else{
          ?>
          <form action="easebuzz/pay.php" method="post" name="Form">
            <div class="form-group col-md-4">
              
              <input type="text" placeholder="Amount to Pay" required id="amount" class="form-control">
            </div>
            <div class="form-group col-md-8">
             
              <input type="text" class="form-control" id="finalAmout" placeholder="Final Amount" disabled value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>">
              <input type="hidden" id="finalAmoutS"  placeholder="Final Amount" name = "amount" value="<?php echo (empty($posted['amount'])) ? '' : $posted['amount'] ?>">
            </div>
            <div class="form-group col-md-12">
              <input  type="text" name="firstname" id="firstname" required value="<?php echo (empty($posted['firstname'])) ? '' : $posted['firstname']; ?>" class="form-control" placeholder="Name"/>              
            </div>
            <div class="form-group col-md-12">
              <input type="text" name="email" placeholder="Email" required id="email" class="form-control" value="<?php echo (empty($posted['email'])) ? '' : $posted['email']; ?>" />              
            </div>
            <div class="form-group col-md-12">
              <input name="phone" placeholder="Mobile Number" required id="phone" class="form-control" value="<?php echo (empty($posted['phone'])) ? '' : $posted['phone']; ?>" />              
            </div>
            <div class="form-group col-md-12">  
              <input type="text" placeholder="Remark" required id="name" class="form-control" name="productinfo" value="<?php echo (empty($posted['productinfo'])) ? '' : $posted['productinfo'] ?>" size="64" />                          
            </div>

            <input type="hidden" name="surl" value="<?php echo (empty($posted['surl'])) ? 'http://localhost/cab/payment.php' : $posted['surl'] ?>" size="64" />
            <input type="hidden" name="furl" value="<?php echo (empty($posted['furl'])) ? 'http://localhost/cab/payment.php' : $posted['furl'] ?>" size="64" />
            
            <button class="btn btn-info" type="submit">
              Pay
            </button>
          </form>
          <?php } ?>
          </div>
        </div>
		
		
		
		<div class="col-lg-5 col-sm-5 address">
          <section class="contact-infos">
            <h4 class="title custom-font text-black">
              ADDRESS
            </h4>
            <address>
            
              <b>Chandramani Housing Society</b>
              <br>
              S. No. 162/2, Road No. 4, 
              <br>
              Tingre Nagar, Vishrantwadi,
			  <br>
			  Pune- 411015.
            </address>
          </section>
         
          <section class="contact-infos">
            <h4>
				Whatsapp No:
            </h4>
            <p>
              <i class="icon-phone">
              </i>
              9822550396
            </p>

            
          </section>
        </div>
		
		
	</div> 
</div> 
</div> 



<!-- ==================== Start inbond links ==================== -->
<div class="width100">
<div class="abovefooter-bg row">
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h3 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Quick Links</h3>
<br>
<ul>
<li><a href="mumbai-ashtavinayak-cabs.html">Mumbai To Ashtavinayak</a></li>
<li><a href="mumbai-airport-to-pune-drop-car-cab-rentals.html">Mumbai To pune Cab</a></li>
<li><a href="pune-to-adlabs-imagica-packages-online-booking.html">Pune To Adlabs Imagica Cab</a></li>
<li><a href="pune-airport-to-alibaug-car-hire-cab-rental-services.html">Pune To Alibag Cab</a></li>
<li><a href="ashtavinayak-darshan-pune.html">Pune To Ashtavinayak Cab</a></li>
<li><a href="ashtavinayak-package-luxury-bus-per-person-booking.html">Pune To Ashtavinayak By Luxury Bus</a></li>
<li><a href="pune-to-aundha-nagnath-cabs-taxi-hire-services.html">Pune To Aundha Nagnath Cab</a></li>
<li><a href="pune-aurangabad-ajanta-ellora-caves-cab-rental-services.html">Pune To Aurangabad Cab</a></li>
<li><a href="pune-to-bhimashankar-cab-taxi-hire-car-rentals.html">Pune To Bhimashankar Cab</a></li>
<li><a href="pune-to-devkund-cabs-taxi-hire-services.html">Pune To Devkund Cab</a></li>
<li><a href="pune-to-mumbai-airport-drop-car-cab-rentals.html">Pune To Mumbai Cab</a></li>

<li><a href="pune-ganapatipule-cab.html">Pune To Ganapatipule Cab </a></li>
<li><a href="pune-goa-packages-cab-car-hire-services.html">Pune To Goa Cab</a></li>
<li><a href="pune-grishneshwar-cab.html">Pune To Grishneshwar Cab</a></li>

</ul>
</div>
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h4 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Tourist Places</h4>
<br>
<ul>
<li><a href="pune-guhagar-tourism-distance-sightseeing.html">Pune To Guhagar Cab</a></li>
<li><a href="pune-to-harihareshwar-cabs-taxi-hire-services.html">Pune To Harihareshwar Cab</a></li>
        <li><a href="pune-to-igatpuri-cabs-taxi-hire-services.html">Pune To Igatpuri Cab</a></li>
<li><a href="pune-kolhapur-cabs-taxi-distance.html">Pune To Kolhapur Cab</a></li>
<li><a href="pune-to-mahabaleshwar-cabs-services.html">Pune To Mahabaleshwar Cab</a></li>
<li><a href="pune-malshej-ghat-cabs-services.html">Pune To Malshej Ghat Cab</a></li>
<li><a href="pune-to-mumbai-cab.html">Pune To Mumbai Airport Cab</a></li>
<li><a href="pune-to-murud-Janjira-cab-taxi-hire-car-rentals.html">Pune To Murud Janjira Cab</a></li>
<li><a href="pune-to-nashik-car-hire-cab-rental-services.html">Pune To Nashik Cab</a></li>
<li><a href="pune-pachgani-tourist-places-visit-car-cab-rental-services.html">Pune To Panchgani Cab</a></li>
<li><a href="pune-pandharpur-darshan-cabs-taxi.html">Pune To Pandharpur Cab</a></li>
<li><a href="pune-panhala-darshan-tourism-cabs.html">Pune To Panhala Fort Cab</a></li>
<li><a href="pune-to-parli-vaijnath-cabs-taxi-hire-services.html">Pune To Parli Vaijnath Cab</a></li>
<li><a href="pune-raigad-fort-darshan-cabs-tourism.html">Pune To Raigad Fort Cab</a></li>
<li><a href="pune-rajgad-fort-cabs-tourism.html">Pune To Rajgad Fort Cab</a></li>

</ul>
</div>
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h4 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Tourist 
Places Near Pune </h4><br>
<ul>

<li><a href="pune-ratnnagiri-cab.html">Pune To Ratnagiri Cab</a></li>
<li><a href="sade-tin-shakti-peeth-darshan-pune.html">Pune To Sadetin Shaktipeeth Cab</a></li>
<li><a href="pune-satara-city-cabs-tourism.html">Pune To Satara Cab</a></li>
<li><a href="pune-satara-kaas-plateau-cabs.html">Pune To Satara Kaas Plateau Cab</a></li>
<li><a href="pune-to-shirdi-cab.html">Pune To Shirdi Cab</a></li>
<li><a href="pune-tarkarli-tourism-beach-cabs-car-rentals-hire-services.html">Pune To Tarkarli Cab</a></li>
<li><a href="pune-to-trimbakeshwar-cab-taxi-hire-car-rentals.html">Pune To Trimbakeshwar Cab</a></li>
<li><a href="pune-tuljapur-darshan-cab.html">Pune To Tuljapur Cab</a></li>
<li><a href="pune-vani-saptashrungi-temple-distance.html">Pune To Vani Saptashrungi Cab</a></li>
<li><a href="privacy-conditions-for-local-and-outstation-trips.html">privacy conditions</a></li>
<li><a href="terms-conditions-for-local-and-outstation-trips.html">terms conditions</a></li>
<li><a href="testimonial.html">testimonial</a></li>
<li><a href="sitemap.html">sitemap</a></li>
</ul>
</div>
</div>
</div>
<!-- ==================== End inbond links ==================== -->


<div class="site-footer">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-4">
<div class="copyright">
<span>Copyright &copy; 2017 <a href="index.html">Pawar Travels</a></span>
</div>
</div> <!-- /.col-md-4 -->
<div class="col-md-4 col-sm-4">
<div class="copyright">

<span>Website & SEO Managed By<a rel="nofollow" href="http://www.sungare.com/">Sungare Technologies</a></span>

</div>
</div> <!-- /.col-md-4 -->
<div class="col-md-4 col-sm-4">
<ul class="social-icons"><li><a href="https://twitter.com/PawarTravels" target="_blank" class="fa fa-twitter"></a></li>
<li><a href="https://www.pinterest.com/pawartravels02/"  target="_blank" class="fa fa-pinterest"></a></li>
<li><a href="
http://in.linkedin.com/in/pawartravels" target="_blank" class="fa fa-linkedin"></a></li>
<li><a href="http://www.flickr.com/groups/pawartravels/" target="_blank" class="fa fa-flickr"></a></li>
<li><a href="https://about.me/pawartravelspune" target="_blank" class="fa fa-rss"></a></li>
</ul>
</div> 
</div> 
</div> 
</div> 

<script>
      var cb = function() {
        var l = document.createElement('link'); l.rel = 'stylesheet';
        l.href = 'css/templatemo-style.css';
        var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(cb);
      else window.addEventListener('load', cb);
    </script>
<script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
<script src="js/vendor/jquery-1.11.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script src="js/vglnk.js"></script>
<script src="js/shareaholic.js"></script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','js/analytics.js','ga');
ga('create', 'UA-52479743-1', 'auto');
ga('send', 'pageview');
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-52479743-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">

    document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    function cp() { return false; }
    function mousehandler(e) {
        var myevent = (isNS) ? e : event;
        var eventbutton = (isNS) ? myevent.which : myevent.button;
        if ((eventbutton == 2) || (eventbutton == 3))
            return false;
    }

    document.oncontextmenu = cp;
    document.onmouseup = cp;

    var isCtrl = false;

    window.onkeyup = function (e) {
        if (e.which == 17)
            isCtrl = false;
    }

    window.onkeydown = function (e) {
        if (e.which == 17)
            isCtrl = true;
        if (((e.which == 85) || (e.which == 65) || (e.which == 88) || (e.which == 67) || (e.which == 86) || (e.which == 83)) && isCtrl == true) {
            return false;
        } 
    }
    document.ondragstart = cp;
    document.images = cp;
</script>

<script type="text/javascript">
  $('#amount').on({
    "keyup change": function(){
      var amt = parseInt(this.value != '' ? this.value : 0),
        tax = 0,
        service = 0,
        final = 0;

        tax = parseInt(amt + (amt * 6 / 100));
        final = parseInt(tax + (tax * 2.6 / 100));

      $('#finalAmout,#finalAmoutS').val(final != 'NaN' || final != Null ? final : '0');      
    }
  })
</script>
</body>
</html>