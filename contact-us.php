<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<meta name="author" content="ajinkya pawar "/>
<meta name="rating" content="general" />
<meta name="distribution" content="global" />
<meta name="doc-type" content="Public"/>
<meta name="distribution" content="Global"/>
<meta name="classification" content="travel"/>
<meta name="geo.country" content="in"/>
<meta name="geo.region" content="IN-Maharashtra" />
<meta name="geo.placename" content="Vishrantwadi, Pune, Maharashtra, India" />
<meta name="geo.position" content="18.574546;73.889210" />
<meta name="ICBM" content="18.574546, 73.889210" />
<meta name="revisit-after" content="7 days">
<meta name=viewport content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property='article:publisher' content='https://www.facebook.com/pawartravels'/></
<meta property='twitter:account_id' content='1442932658'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="p:domain_verify" content="17075096d1a0f34463341555fd6458ad"/>
<meta name="alexaVerifyID" content="g6tWmyWy_Ez39MICPsKCPBPLzJ0"/>
<meta name="google-site-verification" content="Db_TcUpAKNRMACMovA6E306TFww_nBw7gTW2tOMKJyo" />
<meta name="msvalidate.01" content="3CC115F5FAF8B434A49422F90D16FA16" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
<title>Contact Us | Pawar Travels</title>
<meta name="description" content="Contact us we are specialize in mumbai airport to pune Drop service,best Car Rental Services in Pune Guaranteed Lowest Fare ">
<meta name="keywords" content="pune to mumbai, pune to shirdi">
<meta name="robots" content="index, follow">
<style type="text/css">
<!--
.style4 {font-size: 18px}
.style6 {font-size: 18px; font-weight: bold; }
-->
</style>
</head>
<body>
<div class="container">
<div class="row" style="background-color:#fff; color:#42a9f0; padding:10px;">
<div class="col-md-5">
<span style="font-size:16px; margin-top:3px;"><i class="fa fa-phone-square"></i>
<a href="tel:+91-9822550396">+91-9822550396</a>
<a href="tel:+91-9325668721">+91-9325668721</a></span>
</div>
<div class="col-md-4">
<span style="font-size:16px;  margin-top:3px;"><i class="fa fa-envelope-square"></i>	
<a href="mailto:23ajaypawar@gmail.com">23ajaypawar@gmail.com</a></span>
</div>
<div class="col-md-2 col-sm-6">
<ul class="social-icons"><li><span style="font-size:15px;"><a href="https://www.facebook.com/pawartravels" target="_blank" class="fa fa-facebook">
</a></span></li>
<li><span style="font-size:15px;"><a href="https://plus.google.com/u/0/102246497407922758833
" target="_blank" class="fa fa-google-plus"></a></span></li>
<li><span style="font-size:15px;"><a href="https://www.youtube.com/user/pawartravels" target="_blank" class="fa fa-youtube"></a></span></li>
</ul>
</div> 
</div>
</div>
<div class="site-header">
<div class="container">
<div class="main-header">
<div class="row">
<div class="col-md-3 col-sm-5 col-xs-9">
<div class="logo">
<img src="images/logo.png" alt="Pawar Travels" title="Pawar
Travels" class="img-responsive">
</div> 
</div> 
<div class="col-md-9 col-sm-7 col-xs-3">
<div class="main-menu">
<ul class="visible-lg visible-md">
<li><a href="index.html">Home</a></li>
<li><a href="about-us.html">About Us</a></li>
<li><a href="car-rental-packages.html">Packages</a></li>
<li><a href="http://carrentalsinpune.blogspot.in/" target="_blank">Blog</a></li>
<li><a href="enquiry-booking-cab-hire.php">Enquiry</a></li>
<li class="active"><a href="contact-us.php">Contact</a></li>
<li><a href="payment.php">Payment</a></li>

</ul>
<a href="#" class="toggle-menu visible-sm visible-xs">
<i class="fa fa-bars"></i>
</a>
</div> 
</div> 
</div> 
</div> 
<div class="row">
<div class="col-md-12 visible-sm visible-xs">
<div class="menu-responsive">
<ul>
<li><a href="index.html">Home</a></li>
<li><a href="about-us.html">About Us</a></li>
<li class="active"><a href="car-rental-packages.html">Packages</a></li>
<li><a href="http://carrentalsinpune.blogspot.in/" target="_blank">Blog</a></li>
<li><a href="enquiry-booking-cab-hire.php">Enquiry</a></li>
<li><a href="contact-us.php">Contact</a></li>
<li><a href="payment.php">Payment</a></li>
</ul>
</div>
</div> 
</div> 
</div> 
</div> 
<div class="page-top" id="templatemo_about">
</div> 
<div class="contact-page">
<div class="container">
<div class="row">
<h1 class="mainheader">Contact us For Pawar travels</h1>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h2 class="sub-mainheader">&nbsp;</h2>
<p>&nbsp;</p>
<div class="border1"></div><div class="border2"></div><div class="border3"></div>
<div class="col-md-7 col-sm-6 map-wrapper">
<h3 class="widget-title">Our Location</h3>
<div class="map-holder"></div>
<div class="clear">&nbsp;</div>
<div class="clear">&nbsp;
  <h1><span class="style4">We Are Travel Company  Based in Pune Near Pune Airport<br>
    We Specilize in Pune To Mumbai Airport Pickup And Drop services <br>
    One Way A/C Car with Toll .</span></h1>
  <br>
  <h1><span class="style6">Indica 2000/-</span></h1>
  <h1><span class="style4"><strong>Indigo 2200/- </strong><br>
      <strong>Logan/Dezire/ 2400/-</strong></span></h1>
  <h1><span class="style6"> Innova 3300/- </span><strong>.</strong><br>
    <br>  
  </h1>
</div>
<div class="contact-infos">
<ul>
<li></li>
</ul>
</div>
</div>
<div class="col-md-5 col-sm-6">
<h3 class="widget-title">Contact Us</h3>
<div class="contact-form">
<form name="fm" action="#" id="quick_contact" method="post" autocomplete="off" >
<p>
<input name="name" required type="text"  placeholder="Your Name">
</p>
<p>
<input name="email" required type="text"  placeholder="Your Email">
</p>
<p>
<input name="contact" required type="text"  placeholder="Contact Number">
</p>
<p>
<input name="subject1" required type="text"  placeholder="Subject">
</p>
<p>
<textarea name="comment" required  placeholder="Message"></textarea>
</p>
<p>
<input type="submit" name="submit" class="mainBtn" value="Send Message"/>
</p>

<?php
if(isset($_POST['submit'])){
	
	$headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
    $headers .= "From: enquiry@pawartravels.com" . "\r\n" .
            "X-Mailer: PHP/" . phpversion();
            
    $subject = "Pawar Travels";
    $to = "23ajaypawar@gmail.com";
    $message = '<center><table border="1">'
		.'<tr><td>Name</td><td>'.$_POST['name'].'</td></tr>'
		.'<tr><td>Email Address</td><td>'.$_POST['email'].'</td></tr>'
		.'<tr><td>Contact No.</td><td>'.$_POST['contact'].'</td></tr>'
		.'<tr><td>Subject</td><td>'.$_POST['subject1'].'</td></tr>'
		.'<tr><td>Message</td><td>'.$_POST['comment'].'</td></tr>'
		.'</table></center>';
    // echo $message,$subject,$to;
	if(mail($to,$subject,$message,$headers))
	{
		echo 'OK';
	}
	else
	{
		echo 'NO';
	}
}

    
// 	$name=$_POST['name'];
// 	$email=$_POST['email'];
// 	$subject=$_POST['subject1'];
// 	$comment=$_POST['comment'];

	
// 	$recipient="pawartravelspune@gmail.com";

	
// 	$messagebody = "

// ".'Name : '.$name."

// ".'Email : '.$email."

// ".'Subject : '.$subject."

// ".'Comment : '.$comment."


// ";
	
// 	$mail=mail($recipient,$subject,$messagebody);
	
	
// 	if($mail){
		
// 		echo "Send Successfully";
// 	}else{
		
// 		echo "Error in sending email";
// 	}
	


// }

?>

</form>
</div> <!-- /.contact-form -->
<div class="contact-infos">

<p class="style4"><strong>Book Your Cab Call Now </strong></p>
<span class="style4"><strong>Whatsapp No:</strong></span><strong>- <span class="style4">9822550396</span></strong>
<p class="style4"><strong>Mobile No :- 9325668721</strong></p>
<strong class="style4">E-Mail: 23ajaypawar@gmail.com</strong><br><br>
<a class="btn btn-default" href="enquiry-booking-cab-hire.php" role="button">Book Cab Online</a>
<br><br>
<h2 class="style4"><strong>Address:-</strong></h2>
<h2 class="style4">Chandramani Housing Society</h2>
<h2 class="style4">S. No. 162/2, Road No. 4</h2>
<h2 class="style4">Tingre Nagar, Vishrantwadi</h2>
<h2 class="style4">Pune- 411015</h2>
</div>
</div>
</div>
</div>
</div>


<!-- ==================== Start inbond links ==================== -->
<div class="width100">
<div class="abovefooter-bg row">
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h3 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Quick Links</h3>
<br>
<ul>
<li><a href="mumbai-ashtavinayak-cabs.html">Mumbai To Ashtavinayak</a></li>
<li><a href="mumbai-airport-to-pune-drop-car-cab-rentals.html">Mumbai To pune Cab</a></li>
<li><a href="pune-to-adlabs-imagica-packages-online-booking.html">Pune To Adlabs Imagica Cab</a></li>
<li><a href="pune-airport-to-alibaug-car-hire-cab-rental-services.html">Pune To Alibag Cab</a></li>
<li><a href="ashtavinayak-darshan-pune.html">Pune To Ashtavinayak Cab</a></li>
<li><a href="ashtavinayak-package-luxury-bus-per-person-booking.html">Pune To Ashtavinayak By Luxury Bus</a></li>
<li><a href="pune-to-aundha-nagnath-cabs-taxi-hire-services.html">Pune To Aundha Nagnath Cab</a></li>
<li><a href="pune-aurangabad-ajanta-ellora-caves-cab-rental-services.html">Pune To Aurangabad Cab</a></li>
<li><a href="pune-to-bhimashankar-cab-taxi-hire-car-rentals.html">Pune To Bhimashankar Cab</a></li>
<li><a href="pune-to-devkund-cabs-taxi-hire-services.html">Pune To Devkund Cab</a></li>
<li><a href="pune-to-mumbai-airport-drop-car-cab-rentals.html">Pune To Mumbai Cab</a></li>

<li><a href="pune-ganapatipule-cab.html">Pune To Ganapatipule Cab </a></li>
<li><a href="pune-goa-packages-cab-car-hire-services.html">Pune To Goa Cab</a></li>
<li><a href="pune-grishneshwar-cab.html">Pune To Grishneshwar Cab</a></li>

</ul>
</div>
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h4 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Tourist Places</h4>
<br>
<ul>
<li><a href="pune-guhagar-tourism-distance-sightseeing.html">Pune To Guhagar Cab</a></li>
<li><a href="pune-to-harihareshwar-cabs-taxi-hire-services.html">Pune To Harihareshwar Cab</a></li>
        <li><a href="pune-to-igatpuri-cabs-taxi-hire-services.html">Pune To Igatpuri Cab</a></li>
<li><a href="pune-kolhapur-cabs-taxi-distance.html">Pune To Kolhapur Cab</a></li>
<li><a href="pune-to-mahabaleshwar-cabs-services.html">Pune To Mahabaleshwar Cab</a></li>
<li><a href="pune-malshej-ghat-cabs-services.html">Pune To Malshej Ghat Cab</a></li>
<li><a href="pune-to-mumbai-cab.html">Pune To Mumbai Airport Cab</a></li>
<li><a href="pune-to-murud-Janjira-cab-taxi-hire-car-rentals.html">Pune To Murud Janjira Cab</a></li>
<li><a href="pune-to-nashik-car-hire-cab-rental-services.html">Pune To Nashik Cab</a></li>
<li><a href="pune-pachgani-tourist-places-visit-car-cab-rental-services.html">Pune To Panchgani Cab</a></li>
<li><a href="pune-pandharpur-darshan-cabs-taxi.html">Pune To Pandharpur Cab</a></li>
<li><a href="pune-panhala-darshan-tourism-cabs.html">Pune To Panhala Fort Cab</a></li>
<li><a href="pune-to-parli-vaijnath-cabs-taxi-hire-services.html">Pune To Parli Vaijnath Cab</a></li>
<li><a href="pune-raigad-fort-darshan-cabs-tourism.html">Pune To Raigad Fort Cab</a></li>
<li><a href="pune-rajgad-fort-cabs-tourism.html">Pune To Rajgad Fort Cab</a></li>

</ul>
</div>
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h4 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Tourist 
Places Near Pune </h4><br>
<ul>

<li><a href="pune-ratnnagiri-cab.html">Pune To Ratnagiri Cab</a></li>
<li><a href="sade-tin-shakti-peeth-darshan-pune.html">Pune To Sadetin Shaktipeeth Cab</a></li>
<li><a href="pune-satara-city-cabs-tourism.html">Pune To Satara Cab</a></li>
<li><a href="pune-satara-kaas-plateau-cabs.html">Pune To Satara Kaas Plateau Cab</a></li>
<li><a href="pune-to-shirdi-cab.html">Pune To Shirdi Cab</a></li>
<li><a href="pune-tarkarli-tourism-beach-cabs-car-rentals-hire-services.html">Pune To Tarkarli Cab</a></li>
<li><a href="pune-to-trimbakeshwar-cab-taxi-hire-car-rentals.html">Pune To Trimbakeshwar Cab</a></li>
<li><a href="pune-tuljapur-darshan-cab.html">Pune To Tuljapur Cab</a></li>
<li><a href="pune-vani-saptashrungi-temple-distance.html">Pune To Vani Saptashrungi Cab</a></li>
<li><a href="privacy-conditions-for-local-and-outstation-trips.html">privacy conditions</a></li>
<li><a href="terms-conditions-for-local-and-outstation-trips.html">terms conditions</a></li>
<li><a href="testimonial.html">testimonial</a></li>
<li><a href="sitemap.html">sitemap</a></li>
</ul>
</div>
</div>
</div>
<!-- ==================== End inbond links ==================== -->








<div class="site-footer">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-4">
<div class="copyright">
<span>Copyright &copy; 2017 <a href="index.html">Pawar Travels</a></span>
</div>
</div> <!-- /.col-md-4 -->
<div class="col-md-4 col-sm-4">
<div class="copyright">
<span>Website & SEO Managed By<a rel="nofollow" href="http://www.sungare.com/">Sungare Technologies</a></span>
</div>
</div> <!-- /.col-md-4 -->
<div class="col-md-4 col-sm-4"> <ul class="social-icons"><li><a href="https://twitter.com/PawarTravels" target="_blank" class="fa fa-twitter"></a></li>

<li><a href="https://www.pinterest.com/pawartravels02/"  target="_blank" class="fa fa-pinterest"></a></li>
<li><a href="
http://in.linkedin.com/in/pawartravels" target="_blank" class="fa fa-linkedin"></a></li>
<li><a href="http://www.flickr.com/groups/pawartravels/" target="_blank" class="fa fa-flickr"></a></li>
<li><a href="https://about.me/pawartravelspune" target="_blank" class="fa fa-rss"></a></li>
</ul>
</div> <!-- /.col-md-4 -->
</div> <!-- /.row -->
</div> <!-- /.container -->
</div>
<br>
<br>
<script>
      var cb = function() {
        var l = document.createElement('link'); l.rel = 'stylesheet';
        l.href = 'css/templatemo-style.css';
        var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(cb);
      else window.addEventListener('load', cb);
    </script>
<script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
<script src="js/vendor/jquery-1.11.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script src="js/vglnk.js"></script>
<script src="js/shareaholic.js"></script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','js/analytics.js','ga');
ga('create', 'UA-75596292-1', 'auto');
ga('send', 'pageview');
</script>


<!-- Google Map -->
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/vendor/jquery.gmap3.min.js"></script>
<script type="text/javascript">
jQuery(function($){
$('.first-map, .map-holder').gmap3({
marker:{
address: '18.574546,73.889210'
},
map:{
options:{
zoom: 16,
scrollwheel: false,
streetViewControl : true
}
}
});
});
</script>
<script>
function savecontact() {
var formdata = $("#quick_contact").serialize();
//alert (formdata);
$.ajax({
type: "POST",
url: "contact_form/contact.php",
data: formdata,
success: function(response)
{
var result=$.trim(response);
if (result.substr(0,4) == "OK") {
alert("Thank you for Contacting Pawar Travels...!");
}
else
{
alert(result);
}
}
});
}
function ValidateAlpha(evt)
{
var keyCode = (evt.which) ? evt.which : evt.keyCode

if ( ((keyCode >=65)&&(keyCode<=90))||((keyCode >=97)&&(keyCode<=122))||keyCode==9||keyCode==8||keyCode==13||keyCode==32)
{
return true;
}
else
{
return false;
}
}
function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
return false;
return true;
}
</script>

<script type="text/javascript">

    document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    function cp() { return false; }
    function mousehandler(e) {
        var myevent = (isNS) ? e : event;
        var eventbutton = (isNS) ? myevent.which : myevent.button;
        if ((eventbutton == 2) || (eventbutton == 3))
            return false;
    }

    document.oncontextmenu = cp;
    document.onmouseup = cp;

    var isCtrl = false;

    window.onkeyup = function (e) {
        if (e.which == 17)
            isCtrl = false;
    }

    window.onkeydown = function (e) {
        if (e.which == 17)
            isCtrl = true;
        if (((e.which == 85) || (e.which == 65) || (e.which == 88) || (e.which == 67) || (e.which == 86) || (e.which == 83)) && isCtrl == true) {
            return false;
        } 
    }
    document.ondragstart = cp;
    document.images = cp;
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a55ae644b401e45400bf4f4/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
