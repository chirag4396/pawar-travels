<?php
include 'config.php';

error_reporting (E_ALL ^ E_NOTICE);

$post = (!empty($_POST)) ? true : false;

if($post)
{
include 'functions.php';


$Skype = stripslashes($_POST['Skype']);
$name = stripslashes($_POST['name']);
$Designation = stripslashes($_POST['Designation']);
$Brand = stripslashes($_POST['Brand']);
$Contact = stripslashes($_POST['Contact']);
$Country = stripslashes($_POST['Country']);
$Importance = stripslashes($_POST['Importance']);
$message = stripslashes($_POST['message']);
$time = stripslashes($_POST['time']);
$hour = stripslashes($_POST['hour']);
$timeam = stripslashes($_POST['timeam']);

$error = '';

// Check Skype

if(!$Skype)
{
$error .= 'You forgot to enter your Skype ID!<br />';
}

// Check name

if(!$name)
{
$error .= 'You forgot to enter your Name!<br />';
}

// Check Designation

if(!$Designation)
{
$error .= 'You forgot to enter your Designation!<br />';
}

// Check Brand

if(!$Brand)
{
$error .= 'You forgot to enter your Brand!<br />';
}


// Check Contact

if(!$Contact)
{
$error .= 'You forgot to enter your Contact!<br />';
}

// Check Country

if(!$Country)
{
$error .= 'You forgot to enter your Country!<br />';
}

// Check message

if(!$message)
{
$error .= 'You forgot to enter your Message!<br />';
}

// Check time

if(!$time)
{
$error .= 'You forgot to enter your Time!<br />';
}

// Check hour

if(!$hour)
{
$error .= 'You forgot to enter your Hour!<br />';
}


// Check timeam

if(!$timeam)
{
$error .= 'You forgot to enter your AM or PM!<br />';
}

if(!$error)
{
$data = "<table width='500' border='1' cellspacing='0' cellpadding='4'>

<tr>
    <td width='200' height='40'>Skype ID</td>
    <td width='294'>".$Skype."</td>
  </tr>

  <tr>
    <td width='200' height='40'>Name</td>
    <td width='294'>".$name."</td>
  </tr>
  <tr>
    <td width='200' height='40'>Designation</td>
    <td>".$Designation."</td>
  </tr>
  <tr>
    <td width='200' height='40'>Brand / Company</td>
    <td>".$Brand."</td>
  </tr>
  <tr>
    <td width='200' height='40'>Contact</td>
    <td>".$Contact."</td>
  </tr>
  <tr>
    <td width='200' height='40'>Country</td>
    <td>".$Country."</td>
  </tr>
 
  <tr>
    <td width='200' height='40'>Query</td>
    <td>".$message."</td>
  </tr>
  <tr>
    <td width='200' height='40'>Level of Importance </td>
    <td>".$Importance."</td>
  </tr>
  
  
  <tr>
    <td width='200' height='40'>Date Time </td>
    <td>".$time." ".$hour." ".$timeam."</td>
  </tr>
</table>";

$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: Imagine <Inquery@imagine2win.com>' . "\r\n";

$mail = mail(WEBMASTER_EMAIL, "Enquiry From Website Meeting Form...", $data,
     $headers);

if($mail)
{
echo 'OK';
}

}
else
{
echo '<div class="notification_error">'.$error.'</div>';
}

}
?>