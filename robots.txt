#Robots.txt
User-agent: *
Disallow: /temp/   # temp directory
Disallow: /stats/  # stats directory
Disallow: /mycgi/  # mycgi directory
Disallow: /cgi/    # keep robots out of the executable tree
Disallow: /.js     # js file
Disallow: /.inc    # inc file
Disallow: /.css    # css file
Allow: /
Sitemap: http://www.pawartravels.com/sitemap.xml



User-agent: AhrefsBot
User-agent: Baiduspider
User-agent: EasouSpider
User-agent: Ezooms
User-agent: YandexBot
User-agent: MJ12bot
User-agent: SiteSucker
User-agent: HTTrack
Disallow: /