<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
<link rel="canonical" href="enquiry-booking-cab-hire.php"/>
<meta name="author" content="ajinkya pawar "/>
<meta name="rating" content="general" />
<meta name="distribution" content="global" />
<meta name="doc-type" content="Public"/>
<meta name="distribution" content="Global"/>
<meta name="classification" content="travel"/>
<meta name="geo.country" content="in"/>
<meta name="geo.region" content="IN-Maharashtra" />
<meta name="geo.placename" content="Vishrantwadi, Pune, Maharashtra, India" />
<meta name="geo.position" content="18.574546;73.889210" />
<meta name="ICBM" content="18.574546, 73.889210" />
<meta name="revisit-after" content="7 days">
<meta name=viewport content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property='article:publisher' content='https://www.facebook.com/pawartravels'/>
<meta property='twitter:account_id' content='1442932658'>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="p:domain_verify" content="17075096d1a0f34463341555fd6458ad"/>
<meta name="alexaVerifyID" content="g6tWmyWy_Ez39MICPsKCPBPLzJ0"/>
<meta name="google-site-verification" content="Db_TcUpAKNRMACMovA6E306TFww_nBw7gTW2tOMKJyo" />
<meta name="msvalidate.01" content="3CC115F5FAF8B434A49422F90D16FA16" />

<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="images/apple-touch-icon.png" />
<title>Bus Enquiry | Pawar Travels</title>
<meta name="description" content="Enquiry for all type of bus service, Bus Rental service in pune Pawar Travels">
<meta name="keywords" content="Bus hire, Bus rental service, Bus Enquiry">
<meta name="robots" content="index, follow">
<style type="text/css">
<!--
.style2 {font-size: 18px}
-->
</style>
</head>
<body>
<div class="container">
<div class="row" style="background-color:#fff; color:#42a9f0; padding:10px;">
<div class="col-md-5">
<span style="font-size:16px; margin-top:3px;"><i class="fa fa-phone-square"></i>
<a href="tel:+91-9822550396">+91-9822550396</a>
<a href="tel:+91-9325668721">+91-9325668721</a></span>
</div>
<div class="col-md-4">
<span style="font-size:16px;  margin-top:3px;"><i class="fa fa-envelope-square"></i>	<a href="mailto:23ajaypawar@gmail.com
">23ajaypawar@gmail.com</a></span>
</div>
<div class="col-md-2 col-sm-6">
<ul class="social-icons"><li><span style="font-size:15px;"><a href="https://www.facebook.com/pawartravels" target="_blank" class="fa fa-facebook">
</a></span></li>
<li><span style="font-size:15px;"><a href="https://plus.google.com/u/0/102246497407922758833
" target="_blank" class="fa fa-google-plus"></a></span></li>
<li><span style="font-size:15px;"><a href="https://www.youtube.com/user/pawartravels" target="_blank" class="fa fa-youtube"></a></span></li>
</ul>
</div> 
</div>
</div>
<div class="site-header">
<div class="container">
<div class="main-header">
<div class="row">
<div class="col-md-3 col-sm-5 col-xs-9">
<div class="logo">
<img src="images/logo.png" alt="Pawar Travels" title="Pawar Travels" class="img-responsive">
</div> 
</div> 
<div class="col-md-9 col-sm-7 col-xs-3">
<div class="main-menu">
<ul class="visible-lg visible-md">
<li><a href="index.html">Home</a></li>
<li><a href="about-us.html">About Us</a></li>
<li><a href="car-rental-packages.html">Packages</a></li>
<li><a href="http://carrentalsinpune.blogspot.in/" target="_blank">Blog</a></li>
<li class="active"><a href="enquiry-booking-cab-hire.php">Enquiry</a></li>
<li><a href="contact-us.php">Contact</a></li>
<li><a href="payment.php">Payment</a></li>
</ul>
<a href="#" class="toggle-menu visible-sm visible-xs">
<i class="fa fa-bars"></i>
</a>
</div> 
</div> 
</div> 
</div> 
<div class="row">
<div class="col-md-12 visible-sm visible-xs">
<div class="menu-responsive">
<ul>
<li><a href="index.html">Home</a></li>
<li><a href="about-us.html">About Us</a></li>
<li><a href="car-rental-packages.html">Packages</a></li>
<li><a href="http://carrentalsinpune.blogspot.in/" target="_blank">Blog</a></li>
<li class="active"><a href="enquiry-booking-cab-hire.php">Enquiry</a></li>
<li><a href="contact-us.php">Contact</a></li>
<li><a href="payment.php">Payment</a></li>
</ul>
</div>
</div> 
</div> 
</div> 
</div> 
<div class="page-top" id="templatemo_about">
</div>
<div class="contact-page">
<div class="container">
<div class="row">
<div class="border1"></div><div class="border2"></div><div class="border3"></div>
<h1 class="mainheader">Enquiry For Booking Bus for Pawar Travels</h1>
<br>
<h2 class="sub-mainheader">Cab Booking  Enquiry</h2>
<div class="border1"></div><div class="border2"></div><div class="border3"></div>
<div class="col-md-offset-3 col-md-5">
<h3 class="widget-title">Contact Us</h3>
</div>
</div>
</div>
<!--start form code-->
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="widget-bg widget-item"> <br/>
<h3 class="widget-title">Book Your Trip online</h3>
<form name="contactform1" id="index_contact" action="#" method="post">
<p>
<select class="sliderform-select" style="width:85%; padding:1%; margin-bottom:1%" name="journey">
<option value="">I want a bus for </option>
<option value="Round Trip">Round Trip</option>
<option value="One way Trip">One way Trip</option>
</select>
</p>

<p><input name="name" required type="text" id="name" placeholder="Your Name" style="width:85%; padding:1%; margin-bottom:1%"></p>
<p><input name="email" required type="text" id="email" placeholder="Your Email" style="width:85%; padding:1%; margin-bottom:1%"></p>
<p> <input name="contact" required type="text" id="contact" placeholder="Contact No." style="width:85%; padding:1%; margin-bottom:1%"></p>
<p>
<select class="sliderform-select" style="width:85%; padding:1%; margin-bottom:1%" name="cartype">
<option value="">Bus Type </option>
<option value="(17 Seater non ac Tempo Traveller )">(17 Seater non ac Tempo Traveller )</option>
<option value="(20 Seater push back Tempo Traveller)">(20 Seater push back Tempo Traveller)</option>
<option value="(24 Seater non push back Tempo Traveller)">(24 Seater non push back Tempo Traveller)</option>
<option value="(27 Seater hash bank Tempo Traveller)">(27 Seater hash bank Tempo Traveller)</option>
<option value="(32 Seater company body eicher/tata)">(32 Seater company body eicher/tata)</option>
<option value="(32 setter luxeury body eicher/tata )">(32 setter luxeury body eicher/tata )</option>
<option value="(35 setter luxeury push back body eicher )">(35 setter luxeury push back body eicher )</option>
<option value="(50 bus video Coach hash bank eicher/ashok ley)">(50 bus video Coach hash bank eicher/ashok ley)</option>
<option value="(45 seater push back tata )">(45 seater push back tata )</option>
<option value="(17 setter AC Tempo Traveller)">(17 setter AC Tempo Traveller )</option>
<option value="(20 seater push back AC Tempo Traveller )">(20 seater push back AC Tempo Traveller )</option>
<option value="(24 setter AC Tempo Traveller )">(24 setter AC Tempo Traveller )</option>
<option value="(40 seater AC Bus)">(40 seater AC Bus)</option>

</select>
</p>
<p>
<select class="sliderform-select" style="width:85%; padding:1%; margin-bottom:1%" name="cityfrom">
<option value="">From (Select City)</option>
<option value="Pune">Pune</option>
<option value="Pune Airport">Pune Airport</option>
<option value="Mumbai">Mumbai</option>
<option value="Mumbai">Mumbai Airport</option>
</select>
</p>
<p>
<select class="sliderform-select" style="width:85%; padding:1%; margin-bottom:1%" name="cityto">
<option value="">To (Select City)</option>
<option value="Adlabs Imagica Package">Adlabs Imagica Package</option>
<option value="Alibaug">Alibaug</option>
<option value="Ashtavinayak Darshan">Ashtavinayak Darshan</option>
<option value="Aurangabad Ajanta Ellora Caves">Aurangabad Ajanta Ellora Caves </option>
<option value="Bhimashankar">Bhimashankar</option>
<option value="Chiplun">Chiplun</option>
<option value="Dapoli">Dapoli</option>
<option value="Ganapatipule Darshan">Ganapatipule Darshan</option>
<option value="Goa">Goa</option>
<option value="Guhagar">Guhagar</option>
<option value="Harihareshwar">Harihareshwar</option>
<option value="Kolhapur">Kolhapur</option>
<option value="Mahabaleshwar">Mahabaleshwar</option>
<option value="Malshej Ghat">Malshej Ghat</option>
<option value="Mumbai">Mumbai</option>
<option value="Mumbai Airport">Mumbai Airport</option>
<option value="Nashik">Nashik</option>
<option value="Pandharpur Darshan">Pandharpur Darshan</option>
<option value="Panhala Fort">Panhala Fort</option>
<option value="Pune">Pune</option>
<option value="Pune Airport">Pune Airport</option>
<option value="Raigad Fort">Raigad Fort</option>
<option value="Rajgad Fort">Rajgad Fort</option>
<option value="Ratnnagiri">Ratnnagiri</option>
<option value="Satara">Satara</option>
<option value="Satara Kaas Plateau">Satara Kaas Plateau</option>
<option value="Shirdi">Shirdi</option>
<option value="Tarkarli">Tarkarli</option>
<option value="Tuljapur Darshan">Tuljapur Darshan</option>
<option value="Vani Saptashrungi Darshan">Vani Saptashrungi Darshan</option>
</select>
</p>
<p>
<select class="sliderform-select" style="width:85%; padding:1%; margin-bottom:1%" name="Duration">
<option value="">Duration </option>
<option value="Full Day">Full Day</option>
<option value="2 DAY">2 DAY</option>
<option value="3 DAY">3 DAY</option>
<option value="4 DAY">4 DAY </option>
<option value="5 DAY">5 DAY</option>
<option value="6 DAY">6 DAY</option>
<option value="7 DAY">7 DAY</option>
<option value="8 DAY">8 DAY</option>
<option value="9 DAY">9 DAY </option>
<option value="10 DAY">10 DAY</option>
</select>
</p>
<p style="text-align:left !important; padding-left:22px; font-weight:700;">Pickup Date</p>
<p><input name="Pickup_Date" type="date" style="width:85%; padding:1%; margin-bottom:1%"></p>
<p>
<select class="sliderform-select" style="width:85%; padding:1%; margin-bottom:1%" name="Pickup_time">
<option value="">Select Pickup Time</option>
<option value="12:00 AM">12:00 AM</option>
<option value="12:15 AM">12:15 AM</option>
<option value="12:30 AM">12:30 AM</option>
<option value="12:45 AM">12:45 AM</option>
<option value="01:00 AM">01:00 AM</option>
<option value="01:15 AM">01:15 AM</option>
<option value="01:30 AM">01:30 AM</option>
<option value="01:45 AM">01:45 AM</option>
<option value="02:00 AM">02:00 AM</option>
<option value="02:15 AM">02:15 AM</option>
<option value="02:30 AM">02:30 AM</option>
<option value="02:45 AM">02:45 AM</option>
<option value="03:00 AM">03:00 AM</option>
<option value="03:15 AM">03:15 AM</option>
<option value="03:30 AM">03:30 AM</option>
<option value="03:45 AM">03:45 AM</option>
<option value="04:00 AM">04:00 AM</option>
<option value="04:15 AM">04:15 AM</option>
<option value="04:30 AM">04:30 AM</option>
<option value="04:45 AM">04:45 AM</option>
<option value="05:00 AM">05:00 AM</option>
<option value="05:15 AM">05:15 AM</option>
<option value="05:30 AM">05:30 AM</option>
<option value="05:45 AM">05:45 AM</option>
<option value="06:00 AM">06:00 AM</option>
<option value="06:15 AM">06:15 AM</option>
<option value="06:30 AM">06:30 AM</option>
<option value="06:45 AM">06:45 AM</option>
<option value="07:00 AM">07:00 AM</option>
<option value="07:15 AM">07:15 AM</option>
<option value="07:30 AM">07:30 AM</option>
<option value="07:45 AM">07:45 AM</option>
<option value="08:00 AM">08:00 AM</option>
<option value="08:15 AM">08:15 AM</option>
<option value="08:30 AM">08:30 AM</option>
<option value="08:45 AM">08:45 AM</option>
<option value="09:00 AM">09:00 AM</option>
<option value="09:15 AM">09:15 AM</option>
<option value="09:30 AM">09:30 AM</option>
<option value="09:45 AM">09:45 AM</option>
<option value="10:00 AM">10:00 AM</option>
<option value="10:15 AM">10:15 AM</option>
<option value="10:30 AM">10:30 AM</option>
<option value="10:45 AM">10:45 AM</option>
<option value="11:00 AM">11:00 AM</option>
<option value="11:15 AM">11:15 AM</option>
<option value="11:30 AM">11:30 AM</option>
<option value="11:45 AM">11:45 AM</option>
<option value="12:00 PM">12:00 PM</option>
<option value="12:15 PM">12:15 PM</option>
<option value="12:30 PM">12:30 PM</option>
<option value="12:45 PM">12:45 PM</option>
<option value="01:00 PM">01:00 PM</option>
<option value="01:15 PM">01:15 PM</option>
<option value="01:30 PM">01:30 PM</option>
<option value="01:45 PM">01:45 PM</option>
<option value="02:00 PM">02:00 PM</option>
<option value="02:15 PM">02:15 PM</option>
<option value="02:30 PM">02:30 PM</option>
<option value="02:45 PM">02:45 PM</option>
<option value="03:00 PM">03:00 PM</option>
<option value="03:15 PM">03:15 PM</option>
<option value="03:30 PM">03:30 PM</option>
<option value="03:45 PM">03:45 PM</option>
<option value="04:00 PM">04:00 PM</option>
<option value="04:15 PM">04:15 PM</option>
<option value="04:30 PM">04:30 PM</option>
<option value="04:45 PM">04:45 PM</option>
<option value="05:00 PM">05:00 PM</option>
<option value="05:15 PM">05:15 PM</option>
<option value="05:30 PM">05:30 PM</option>
<option value="05:45 PM">05:45 PM</option>
<option value="06:00 PM">06:00 PM</option>
<option value="06:15 PM">06:15 PM</option>
<option value="06:30 PM">06:30 PM</option>
<option value="06:45 PM">06:45 PM</option>
<option value="07:00 PM">07:00 PM</option>
<option value="07:15 PM">07:15 PM</option>
<option value="07:30 PM">07:30 PM</option>
<option value="07:45 PM">07:45 PM</option>
<option value="08:00 PM">08:00 PM</option>
<option value="08:15 PM">08:15 PM</option>
<option value="08:30 PM">08:30 PM</option>
<option value="08:45 PM">08:45 PM</option>
<option value="09:00 PM">09:00 PM</option>
<option value="09:15 PM">09:15 PM</option>
<option value="09:30 PM">09:30 PM</option>
<option value="09:45 PM">09:45 PM</option>
<option value="10:00 PM">10:00 PM</option>
<option value="10:15 PM">10:15 PM</option>
<option value="10:30 PM">10:30 PM</option>
<option value="10:45 PM">10:45 PM</option>
<option value="11:00 PM">11:00 PM</option>
<option value="11:15 PM">11:15 PM</option>
<option value="11:30 PM">11:30 PM</option>
<option value="11:45 PM">11:45 PM</option>
</select>
</p>
<p><input name="Pickup_address" required type="text" placeholder="Pickup Address" style="width:85%; padding:1%; margin-bottom:1%"></p>
<p><input name="drop_address" required type="text" placeholder="Drop Address" style="width:85%; padding:1%; margin-bottom:1%"></p>
<p><textarea name="message" id="message" placeholder="Your Message" style="width:85%; padding:1%; margin-bottom:1%"></textarea></p>
<!--<input type="submit" class="mainBtn" id="submit" value="Submit" >-->
<input type="submit" name="submit" class="mainBtn" value="Submit"/><br>

<?php
if(isset($_POST['submit'])){
	
	if(!empty($_POST)){
	$name=$_POST['name'];
        $journey=$_POST['journey'];
	$email=$_POST['email'];
	$contact=$_POST['contact'];
	$cartype=$_POST['cartype'];
	$cityfrom=$_POST['cityfrom'];
	$cityto=$_POST['cityto'];
        $Pickup_Date=$_POST['Pickup_Date'];
        $Pickup_time=$_POST['Pickup_time'];
        $Pickup_address=$_POST['Pickup_address'];
        $drop_address=$_POST['drop_address'];
        $message=$_POST['message'];
	$Duration=$_POST['Duration'];
	
	$subject="Enquiry From Pawar Travels";
	
	$recipient="pawartravelspune@gmail.com";

	
	$messagebody = "

".'Name : '.$name."

".'I want a car for : '.$journey."

".'Email : '.$email."

".'Contact : '.$contact."

".'Cartype : '.$cartype."

".'Cityfrom : '.$cityfrom."

".'Cityto : '.$cityto."

".'Pickup Date : '.$Pickup_Date."

".'Pickup Time : '.$Pickup_time."

".'Pickup Address : '.$Pickup_address."

".'Drop Address : '.$drop_address."

".'Your Message : '.$message."

".'Duration : '.$Duration."

";
   

	
	
	
	$mail=mail($recipient,$subject,$messagebody);
	
	
	if($mail){
		
		echo "Send Successfully";
	}else{
		
		echo "Error in sending email";
	}
	
}else{
	
	echo "Please Fill All The Fields";
}


}

?>




</form>
<!--end form code-->


<p class="style2">Book  Cab Call Now </p>
<span style="font-size:16px; margin-top:3px;"><i class="fa fa-phone-square"></i>
9822550396 </span>
<span style="font-size:16px; margin-top:3px;"><i class="fa fa-phone-square"></i>
9325668721 </span><br>
<strong>E-Mail:</strong><a href="mailto:23ajaypawar@gmail.com
"><strong> 23ajaypawar@gmail.com</strong></a></div> 
<!-- /.contact-form -->
</div> <!-- /.contact-form -->
</div>
</div>
</div>



<!-- ==================== Start inbond links ==================== -->
<div class="width100">
<div class="abovefooter-bg row">
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h3 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Quick Links</h3>
<br>
<ul>
<li><a href="mumbai-ashtavinayak-cabs.html">Mumbai To Ashtavinayak</a></li>
<li><a href="mumbai-airport-to-pune-drop-car-cab-rentals.html">Mumbai To pune Cab</a></li>
<li><a href="pune-to-adlabs-imagica-packages-online-booking.html">Pune To Adlabs Imagica Cab</a></li>
<li><a href="pune-airport-to-alibaug-car-hire-cab-rental-services.html">Pune To Alibag Cab</a></li>
<li><a href="ashtavinayak-darshan-pune.html">Pune To Ashtavinayak Cab</a></li>
<li><a href="ashtavinayak-package-luxury-bus-per-person-booking.html">Pune To Ashtavinayak By Luxury Bus</a></li>
<li><a href="pune-to-aundha-nagnath-cabs-taxi-hire-services.html">Pune To Aundha Nagnath Cab</a></li>
<li><a href="pune-aurangabad-ajanta-ellora-caves-cab-rental-services.html">Pune To Aurangabad Cab</a></li>
<li><a href="pune-to-bhimashankar-cab-taxi-hire-car-rentals.html">Pune To Bhimashankar Cab</a></li>
<li><a href="pune-to-devkund-cabs-taxi-hire-services.html">Pune To Devkund Cab</a></li>
<li><a href="pune-to-mumbai-airport-drop-car-cab-rentals.html">Pune To Mumbai Cab</a></li>

<li><a href="pune-ganapatipule-cab.html">Pune To Ganapatipule Cab </a></li>
<li><a href="pune-goa-packages-cab-car-hire-services.html">Pune To Goa Cab</a></li>
<li><a href="pune-grishneshwar-cab.html">Pune To Grishneshwar Cab</a></li>

</ul>
</div>
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h4 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Tourist Places</h4>
<br>
<ul>
<li><a href="pune-guhagar-tourism-distance-sightseeing.html">Pune To Guhagar Cab</a></li>
<li><a href="pune-to-harihareshwar-cabs-taxi-hire-services.html">Pune To Harihareshwar Cab</a></li>
        <li><a href="pune-to-igatpuri-cabs-taxi-hire-services.html">Pune To Igatpuri Cab</a></li>
<li><a href="pune-kolhapur-cabs-taxi-distance.html">Pune To Kolhapur Cab</a></li>
<li><a href="pune-to-mahabaleshwar-cabs-services.html">Pune To Mahabaleshwar Cab</a></li>
<li><a href="pune-malshej-ghat-cabs-services.html">Pune To Malshej Ghat Cab</a></li>
<li><a href="pune-to-mumbai-cab.html">Pune To Mumbai Airport Cab</a></li>
<li><a href="pune-to-murud-Janjira-cab-taxi-hire-car-rentals.html">Pune To Murud Janjira Cab</a></li>
<li><a href="pune-to-nashik-car-hire-cab-rental-services.html">Pune To Nashik Cab</a></li>
<li><a href="pune-pachgani-tourist-places-visit-car-cab-rental-services.html">Pune To Panchgani Cab</a></li>
<li><a href="pune-pandharpur-darshan-cabs-taxi.html">Pune To Pandharpur Cab</a></li>
<li><a href="pune-panhala-darshan-tourism-cabs.html">Pune To Panhala Fort Cab</a></li>
<li><a href="pune-to-parli-vaijnath-cabs-taxi-hire-services.html">Pune To Parli Vaijnath Cab</a></li>
<li><a href="pune-raigad-fort-darshan-cabs-tourism.html">Pune To Raigad Fort Cab</a></li>
<li><a href="pune-rajgad-fort-cabs-tourism.html">Pune To Rajgad Fort Cab</a></li>

</ul>
</div>
<div class="col-xs-offset-1 col-md-3 col-sm-3 borderright">
<h4 style="font-size:14px; text-transform:uppercase; color:#fff; margin:5px 0px 5px 0px;font-weight:bold;">Tourist 
Places Near Pune </h4><br>
<ul>

<li><a href="pune-ratnnagiri-cab.html">Pune To Ratnagiri Cab</a></li>
<li><a href="sade-tin-shakti-peeth-darshan-pune.html">Pune To Sadetin Shaktipeeth Cab</a></li>
<li><a href="pune-satara-city-cabs-tourism.html">Pune To Satara Cab</a></li>
<li><a href="pune-satara-kaas-plateau-cabs.html">Pune To Satara Kaas Plateau Cab</a></li>
<li><a href="pune-to-shirdi-cab.html">Pune To Shirdi Cab</a></li>
<li><a href="pune-tarkarli-tourism-beach-cabs-car-rentals-hire-services.html">Pune To Tarkarli Cab</a></li>
<li><a href="pune-to-trimbakeshwar-cab-taxi-hire-car-rentals.html">Pune To Trimbakeshwar Cab</a></li>
<li><a href="pune-tuljapur-darshan-cab.html">Pune To Tuljapur Cab</a></li>
<li><a href="pune-vani-saptashrungi-temple-distance.html">Pune To Vani Saptashrungi Cab</a></li>
<li><a href="privacy-conditions-for-local-and-outstation-trips.html">privacy conditions</a></li>
<li><a href="terms-conditions-for-local-and-outstation-trips.html">terms conditions</a></li>
<li><a href="testimonial.html">testimonial</a></li>
<li><a href="sitemap.html">sitemap</a></li>
</ul>
</div>
</div>
</div>
<!-- ==================== End inbond links ==================== -->









<div class="site-footer">
<div class="container">
<div class="row">
<div class="col-md-4 col-sm-4">
<div class="copyright">
<span>Copyright &copy; 2017 <a href="index.html">Pawar Travels</a></span>
</div>
</div> 
<div class="col-md-4 col-sm-4">
<div class="copyright">
<span>Website & SEO Managed By<a rel="nofollow" href="http://www.sungare.com/">Sungare Technologies</a></a></span>
</div>
</div> 
<div class="col-md-4 col-sm-4"> <ul class="social-icons"><li><a href="https://twitter.com/PawarTravels" target="_blank" class="fa fa-twitter"></a></li>

<li><a href="https://www.pinterest.com/pawartravels02/"  target="_blank" class="fa fa-pinterest"></a></li>
<li><a href="
http://in.linkedin.com/in/pawartravels" target="_blank" class="fa fa-linkedin"></a></li>
<li><a href="http://www.flickr.com/groups/pawartravels/" target="_blank" class="fa fa-flickr"></a></li>
<li><a href="https://about.me/pawartravelspune" target="_blank" class="fa fa-rss"></a></li>
</ul>
</div> 
</div> 
</div> 
</div>

<br>
<br>
<script>
      var cb = function() {
        var l = document.createElement('link'); l.rel = 'stylesheet';
        l.href = 'css/templatemo-style.css';
        var h = document.getElementsByTagName('head')[0]; h.parentNode.insertBefore(l, h);
      };
      var raf = requestAnimationFrame || mozRequestAnimationFrame ||
          webkitRequestAnimationFrame || msRequestAnimationFrame;
      if (raf) raf(cb);
      else window.addEventListener('load', cb);
    </script>
<script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
<script src="js/vendor/jquery-1.11.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script src="js/vglnk.js"></script>
<script src="js/shareaholic.js"></script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','js/analytics.js','ga');
ga('create', 'UA-75596292-1', 'auto');
ga('send', 'pageview');
</script>

<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
<script src="js/bootstrap.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script src="js/vglnk.js"></script>
<script src="js/shareaholic.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="js/vendor/jquery.gmap3.min.js"></script>
<script type="text/javascript">
jQuery(function($){
$('.first-map, .map-holder').gmap3({
marker:{
address: '16.8496189,96.1288854'
},
map:{
options:{
zoom: 16,
scrollwheel: false,
streetViewControl : true
}
}
});
});
</script>
<script>
function ValidateAlpha(evt)
{
var keyCode = (evt.which) ? evt.which : evt.keyCode
if ( ((keyCode >=65)&&(keyCode<=90))||((keyCode >=97)&&(keyCode<=122))||keyCode==9||keyCode==8||keyCode==13||keyCode==32)
{
return true;
}
else
{
return false;
}
}

function isNumberKey(evt)
{
var charCode = (evt.which) ? evt.which : event.keyCode
if (charCode > 31 && (charCode < 48 || charCode > 57))
return false;

return true;
}
</script>


<!-- Google Code start for contact form Conversion Page -->

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 982956828;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "3tJFCNOn92AQnPba1AM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/982956828/?label=3tJFCNOn92AQnPba1AM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Google Code end for contact form Conversion Page -->


<script type="text/javascript">

    document.onkeypress = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    document.onmousedown = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    document.onkeydown = function (event) {
        event = (event || window.event);
        if (event.keyCode === 123) {
            return false;
        } 
    };

    function cp() { return false; }
    function mousehandler(e) {
        var myevent = (isNS) ? e : event;
        var eventbutton = (isNS) ? myevent.which : myevent.button;
        if ((eventbutton == 2) || (eventbutton == 3))
            return false;
    }

    document.oncontextmenu = cp;
    document.onmouseup = cp;

    var isCtrl = false;

    window.onkeyup = function (e) {
        if (e.which == 17)
            isCtrl = false;
    }

    window.onkeydown = function (e) {
        if (e.which == 17)
            isCtrl = true;
        if (((e.which == 85) || (e.which == 65) || (e.which == 88) || (e.which == 67) || (e.which == 86) || (e.which == 83)) && isCtrl == true) {
            return false;
        } 
    }
    document.ondragstart = cp;
    document.images = cp;
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a55ae644b401e45400bf4f4/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
